﻿using System;

namespace Otus.Teaching.PromoCodeFactory.GraphQL.Models
{
    public class DeleteCustomer
    {
        public Guid Id { get; set; }
    }
}
