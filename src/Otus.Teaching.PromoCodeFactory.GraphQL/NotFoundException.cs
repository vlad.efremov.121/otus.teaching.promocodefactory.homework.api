﻿using System;

namespace Otus.Teaching.PromoCodeFactory.GraphQL
{
    public class NotFoundException : Exception
    {
        public NotFoundException() : base("Not found") { }
    }
}

